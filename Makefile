OBJS = main.o listener.o protocolMsg.o parksharkDb.o
CC = clang++
DEBUG = -g
CFLAGS = -Wall -c -std=c++11 $(DEBUG)
LFLAGS = -Wall -lmysqlcppconn
EXEC = listener

$(EXEC): $(OBJS)
	$(CC) $(LFLAGS) $(OBJS) -o $(EXEC)
	mkdir -p output/bin output/obj
	mv $(EXEC) output/bin/
	mv $(OBJS) output/obj/

rebuild: clean $(EXEC)

main.o: listener.o
	$(CC) $(CFLAGS) main.cpp

listener.o: protocolMsg.o parksharkDb.o listener.hpp
	$(CC) $(CFLAGS) listener.cpp

protocolMsg.o: msg/protocolMsg.hpp
	$(CC) $(CFLAGS) msg/protocolMsg.cpp

parksharkDb.o: parksharkDb.hpp
	$(CC) $(CFLAGS) parksharkDb.cpp

clean:
	\rm -f *~ *.o $(EXEC)
	\rm -rf output/
