#ifndef PARKSHARKDB__HPP
#define PARKSHARKDB__HPP

#include <cppconn/driver.h>
#include <cppconn/connection.h>

class ParkSharkDb
{
	sql::Driver *_driver;
	sql::Connection *_dbConn;
	
	public:
		ParkSharkDb();
		~ParkSharkDb();
		
		void Connect();
		bool IsConnected();
		
		void UpdateCars(int lotId, int carChange);
	
	private:
		void Transaction(const char* query);
		void PersistConnection();
};

#endif
