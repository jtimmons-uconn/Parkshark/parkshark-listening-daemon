#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>
#include <vector>

#include "listener.hpp"
#include "msg/protocolMsg.hpp"
#include "parksharkDb.hpp"

Listener::Listener()
{
	_db = new ParkSharkDb();
}

Listener::~Listener()
{
	delete _db;
}

void Listener::Run()
{
	int sockfd, n;
	char buffer[BUFFER_SIZE];
	struct sockaddr_in serv_addr, cli_addr;
	socklen_t len;

	// Initialize server address
	memset(&serv_addr, 0, sizeof(serv_addr));
	serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = INADDR_ANY;
	serv_addr.sin_port = htons(PORT_NO);

	// Create new socket descriptor
	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0)
	{
		perror("Could not create socket descriptor\n");
		exit(1);
	}

	// Bind socket to server address
	if(bind(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) == -1)
	{
		perror("Could not bind socket (Probably a permissions issue)\n");
		exit(1);
	}

	// Loop forever
	_running = true;
	while(_running)
	{
		len = sizeof(cli_addr);
		n = recvfrom(sockfd, buffer, BUFFER_SIZE, 0, (struct sockaddr *)&cli_addr, &len);

		processmesg(buffer, n); // A message was correctly received
	}
}

void Listener::Stop()
{
	_running = false; // Useful for multithreaded implementations
}

void Listener::processmesg(char *mesg, int len)
{
	printMesg(mesg, len);

	std::vector<unsigned char> v(mesg, mesg + len);
	ProtocolMsg msg(v);
	if(msg.IsValid())
	{
		_db->UpdateCars(msg.GetSenderId(), msg.GetCarChange());
		logMessage();
	}
}

void Listener::printMesg(char *mesg, int len)
{
	printf("Message Received: ");
	for(int i=0; i<len; i++)
		printf("%02X ", (unsigned char)mesg[i]);
	printf("\n");
}

void Listener::logMessage()
{
}
