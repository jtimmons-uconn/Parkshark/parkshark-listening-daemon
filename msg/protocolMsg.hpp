#ifndef __PROTOCOLMSG_HPP_
#define __PROTOCOLMSG_HPP_

#include <vector>
#include <list>

enum ALFC
{
  CAR_ENTER = 0,
  CAR_LEAVE,
  STATUS_CODE,
  RESERVED
};

enum StatusCode
{
  OK = 0,
};

class MsgByte
{
  const static unsigned char MAX_VALUE = 64;

  unsigned char _value;
  ALFC _alfc;

  public:
    MsgByte(unsigned char byte);
    MsgByte(ALFC code, unsigned char value);

    unsigned char GetByte();
    ALFC GetCode();
    unsigned char GetValue();

    bool IncrementValue();
    void Print();
};

class ProtocolMsg
{
  const static unsigned char FRM_STRT=0xA0;
	const static unsigned char FRM_END=0x0A;
  unsigned short _lotId;
  std::list<MsgByte> _msgBytes;
  bool _isValid;

  public:
    ProtocolMsg(std::vector<unsigned char> msg);
    ProtocolMsg(unsigned short lotId);

    void CarEntered();
    void CarLeft();
    void AddStatusCode(StatusCode code);

    bool IsValid();
    int GetSenderId();
    std::vector<unsigned char> GetBytes();
    int GetCarChange();

    void Print();

  private:
    bool ValidityCheck(std::vector<unsigned char> msg);
    void CarEnteredOrLeft(ALFC alfc);
    void InsertElement(const MsgByte byte);
};

#endif
