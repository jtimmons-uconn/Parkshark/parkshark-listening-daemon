#include <iostream>

#include "protocolMsg.hpp"

ProtocolMsg::ProtocolMsg(std::vector<unsigned char> msg)
{
  if(!ValidityCheck(msg))
    return;

  // Parse message
  _lotId = msg[1];
  for(int i=2; i < msg.size() - 1; i++)
  {
    MsgByte byt(msg[i]);
    InsertElement(byt);
  }
}

ProtocolMsg::ProtocolMsg(unsigned short lotId)
{
  _lotId = lotId;
}

void ProtocolMsg::CarEntered()
{
  CarEnteredOrLeft(CAR_ENTER);
}

void ProtocolMsg::CarLeft()
{
  CarEnteredOrLeft(CAR_LEAVE);
}

void ProtocolMsg::CarEnteredOrLeft(ALFC alfc)
{
  // Need to try to increment an existing byte
  // If it is full or does not exist, create a new one
  bool incrementedExisting = false;
  for(MsgByte& byt : _msgBytes)
  {
    if(byt.GetCode() == alfc)
      incrementedExisting = byt.IncrementValue();
  }

  // Create a new message byte
  if(!incrementedExisting)
  {
    MsgByte byt(alfc, 1);
    InsertElement(byt);
  }
}

void ProtocolMsg::AddStatusCode(StatusCode code)
{
  MsgByte byt(STATUS_CODE, code);
  InsertElement(byt);
}

bool ProtocolMsg::IsValid()
{
  return _isValid;
}

int ProtocolMsg::GetSenderId()
{
  return _lotId;
}

std::vector<unsigned char> ProtocolMsg::GetBytes()
{
  std::vector<unsigned char> bytes;
  unsigned char strt = FRM_STRT, end = FRM_END;

  bytes.push_back(strt);
  bytes.push_back(_lotId);

  for(MsgByte& byt : _msgBytes)
    bytes.push_back(byt.GetByte());

  bytes.push_back(end);
  return bytes;
}

int ProtocolMsg::GetCarChange()
{
  int i = 0;
  for(MsgByte& byt : _msgBytes)
  {
    if(byt.GetCode() == CAR_ENTER)
      i += byt.GetValue();
    else if (byt.GetCode() == CAR_LEAVE)
      i -= byt.GetValue();
  }

  return i;
}

bool ProtocolMsg::ValidityCheck(std::vector<unsigned char> msg)
{
  // Check frame delimiters
  _isValid = (msg.front() == FRM_STRT) && (msg.back() == FRM_END);
  return _isValid;
}

void ProtocolMsg::InsertElement(const MsgByte byte)
{
  _msgBytes.push_back(byte);
}

void ProtocolMsg::Print()
{
  std::cout << "Id: " << _lotId << "\n";
  for(MsgByte& byt : _msgBytes)
    byt.Print();
}




MsgByte::MsgByte(unsigned char byte)
{
  char alfcMask = ~((1 << 6) - 1);
  char valueMask = (1 << 6) - 1;

  _alfc = (ALFC)((byte & alfcMask) >> 6);
  _value = (byte & valueMask);
}

MsgByte::MsgByte(ALFC code, unsigned char value)
{
  _alfc = code;
  _value = value;
}

unsigned char MsgByte::GetByte()
{
  unsigned char alfcMask = 0x02, valueMask = 0x3F;

  unsigned char alfc = (_alfc & alfcMask) << 6;
  unsigned char value = (_value & valueMask);

  return (alfc + value);
}

ALFC MsgByte::GetCode()
{
  return _alfc;
}

unsigned char MsgByte::GetValue()
{
  return _value;
}

bool MsgByte::IncrementValue()
{
  if(_value == MAX_VALUE)
    return false;

  _value++;
  return true;
}

void MsgByte::Print()
{
  std::cout << "ALFC: " << _alfc << ", Value: " << (int)_value << "\n";
}
