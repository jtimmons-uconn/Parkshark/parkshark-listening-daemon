#ifndef LISTENER__HPP
#define LISTENER__HPP

#include "parksharkDb.hpp"

class Listener
{
	const static int BUFFER_SIZE = 256;
	const static int PORT_NO = 2112;
	
	ParkSharkDb *_db;
	bool _running;
	
	public:
		Listener();
		~Listener();
		
		void Run();
		void Stop();
		
	private:
		void processmesg(char *mesg, int len);
		void printMesg(char *mesg, int len);
		void logMessage();
};

#endif
