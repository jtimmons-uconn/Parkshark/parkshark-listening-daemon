#include "parksharkDb.hpp"

#include <string>
#include <sstream>
#include <iostream>

#include <cppconn/driver.h>
#include <cppconn/connection.h>
#include <cppconn/exception.h>
#include <cppconn/statement.h>

ParkSharkDb::ParkSharkDb()
{
	_driver = get_driver_instance();
	Connect();	
}

ParkSharkDb::~ParkSharkDb()
{
	delete _dbConn;
}

void ParkSharkDb::Connect()
{
	try
	{
		_dbConn = _driver->connect("localhost", "justin_parkshark", "parkshark");
		_dbConn->setSchema("justin_parkshark");
	}
	catch(sql::SQLException &e)
	{
		std::cerr << "Could not open connection to ParkShark database\n";
		std::cerr << e.what();
		exit(1);
	}
}

bool ParkSharkDb::IsConnected()
{
	return !_dbConn->isClosed();
}
		
void ParkSharkDb::UpdateCars(int lotId, int carChange)
{
	std::stringstream s;
	s << "UPDATE lots SET carsInLot=carsInLot+" << carChange << " WHERE id=" << lotId << ";\n";
	
	Transaction(s.str().c_str());
}

void ParkSharkDb::Transaction(const char* query)
{
	try
	{
		PersistConnection();
		
		sql::Statement *stmt = _dbConn->createStatement();
		stmt->execute(query);
		
		delete stmt;
	}
	
	catch(sql::SQLException &e)
	{
		std::cerr << "Could not complete query to ParkShark database\n";
		std::cerr << e.what();
		exit(1);
	}
}

void ParkSharkDb::PersistConnection()
{
	if(!IsConnected())
		Connect();
}
